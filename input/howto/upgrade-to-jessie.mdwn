# Upgrade from wheezy to jessie

Make sure to coordinate with whoever uses the host, be it the buildd
operator, the QA team, the ftp team or just announce it if it's a
general developer box.

on buildds:

	buildds want moving to the new puppetized setup!

upgrade stuff:

* answer question to libc6 "Restart services automaticaly" with "yes"
* say no to other other questions.

commands:

	sed -i "s#wheezy#jessie#g" /etc/apt/sources.list.d/debian.list /etc/apt/sources.list.d/security.list &&
	dpkg --clear-avail &&
	if [ -e /etc/apt/sources.list.d/buildd.debian.org.list ]; then
		sed -i "s#wheezy#jessie#g" /etc/apt/sources.list.d/buildd.debian.org.list
	fi &&
	apt-get update &&
	apt-get install dpkg apt samhain pinentry-curses &&
	service samhain stop &&
	apt-get dist-upgrade &&
	rm -f /var/state/samhain/samhain_file /var/lib/samhain/samhain_file &&
	samhain --foreground -t init -p none -s none -l none -m none &&
	(puppet agent -t || true) &&
	apt-get update &&
	apt-get dist-upgrade &&
	apt-get purge ruby1.8 libffi5:amd64 emacs23-nox libruby1.9.1 libfilesystem-ruby1.9.1 libruby1.8 emacs23-common ruby1.9.1 emacs23-bin-common &&
	apt-get --purge autoremove &&
	while [ "$(deborphan -n | wc -l)" -gt 0 ] ; do apt-get purge $(deborphan -n); done &&
	dpkg --clear-avail &&
	apt-get clean


purge removed packages

	dpkg --get-selections | awk '$2=="deinstall" {print $1}' &&
	echo "really purge these [y/N]?" && read ans && [ "$ans" = "y" ] && dpkg --purge `dpkg --get-selections | awk '$2=="deinstall" {print $1}'` &&
	echo "These are not at install:" && dpkg --get-selections | awk '$2!="install" {print $1}'

more clean ups:

	/usr/lib/nagios/plugins/dsa-check-packages | tr -d ,

	(apt-get purge them)

	apt-get --purge autoremove &&
	while [ "$(deborphan -n | wc -l)" -gt 0 ] ; do apt-get purge $(deborphan -n); done

	(puppet agent -t || true) && (puppet agent -t || true)

nfs kernel module fu:
	grep '^nfs$' /etc/modules && ! grep '^nfsv4$' /etc/modules && echo nfsv4 | tee -a /etc/modules

apache cleanup:

	mkdir /etc/apache2/conf.d.WHEEZY &&
	cd /etc/apache2/conf.d &&
	  for i in *; do
	    diff -u $i ../conf-enabled/$i.conf && mv -v $i ../conf.d.WHEEZY/
	  done &&
	  cd .. &&
	  rmdir conf.d

	per vhost:
	  move sites-available file to foo.conf,
          if they exist: move sites-available/RCS and sites-staging files to foo.conf,v and foo.conf
	  remove dangling symlink in sites-enabled,
	  a2ensite foo
	  replace all order allow,deny, allow from all with
	    Require all granted

Add the new ed25519 ssh host key:

	run

	  echo; echo "cat << EOF | ldapmodify -ZZ -x -D uid="\$USER",ou=users,dc=debian,dc=org -W -h db.debian.org"; echo "dn: host=$(hostname),ou=hosts,dc=debian,dc=org"; echo "changetype: modify"; echo "add: sshRSAHostKey"; echo "sshRSAHostKey: $(cat /etc/ssh/ssh_host_ed25519_key.pub)"; echo; echo "EOF"; echo

	and paste it on draghi.
	
update dsa-nagios.git (add host to jessie hostgroup)

re-init samhain and finish with a reboot

	(puppet agent -t || true) &&
	(puppet agent -t || true) &&
	(puppet agent -t || true) &&
	samhain --foreground -t update -p none -s none -l none -m none &&
	/sbin/reboot
