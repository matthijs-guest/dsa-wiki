# Upgrade from stretch to buster:

Make sure to coordinate with whoever uses the host, be it the buildd
operator, the QA team, the ftp team or just announce it on IRC if it's a
general developer box.

upgrade stuff:

* answer question to libc6 "Restart services automaticaly" with "yes"
* say no (or "keep the currently installed version") to other questions.

first re-enable module loading, as new modules are needed to restart ferm (due to nftables):

	sed -i "s#echo 1#echo 0#" /etc/rc.local &&
	reboot < /dev/null

commands:

	sed -i "s#stretch#buster#g" /etc/apt/sources.list.d/debian.list /etc/apt/sources.list.d/security.list &&
	dpkg --clear-avail &&
	apt-get update &&
	service samhain stop &&
	rm -f /var/state/samhain/samhain_file /var/lib/samhain/samhain_file &&
	apt-get install dpkg apt samhain &&
	service samhain stop &&
	apt-get dist-upgrade &&
	service samhain stop &&
	rm -f /var/state/samhain/samhain_file /var/lib/samhain/samhain_file &&
	samhain --foreground -t init -p none -s none -l none -m none &&
	(puppet agent -t || true) &&
	: sometimes puppet resets our sources back to stretch.  insist &&
	sed -i "s#stretch#buster#g" /etc/apt/sources.list.d/debian.list /etc/apt/sources.list.d/security.list &&
	apt-get update &&
	apt-get dist-upgrade &&
	apt-get --purge autoremove &&
	while [ "$(deborphan -n | wc -l)" -gt 0 ] ; do apt-get purge $(deborphan -n); done &&
	dpkg --clear-avail &&
	apt-get clean

purge removed packages

	dpkg --get-selections | awk '$2=="deinstall" {print $1}' &&
	echo "really purge these [y/N]?" && read ans && [ "$ans" = "y" ] && dpkg --purge `dpkg --get-selections | awk '$2=="deinstall" {print $1}'` &&
	echo "These are not at install:" && dpkg --get-selections | awk '$2!="install" {print $1}'

more clean ups:

	apt-get update &&
	/usr/lib/nagios/plugins/dsa-check-packages | tr -d ,

	(apt-get purge them)

	apt-get --purge autoremove &&
	while [ "$(deborphan -n | wc -l)" -gt 0 ] ; do apt-get purge $(deborphan -n); done

	(puppet agent -t || true) && (puppet agent -t || true)

update dsa-nagios.git (add host to buster hostgroup)

re-init samhain and finish with a reboot

	(puppet agent -t || true) &&
	(puppet agent -t || true) &&
	(puppet agent -t || true) &&
	samhain --foreground -t update -p none -s none -l none -m none &&
	dsa-update-apt-status -f &&
	reboot < /dev/null

consider more cleanup

	apt-get autoremove &&
	(puppet agent -t || true) &&
	samhain --foreground -t update -p none -s none -l none -m none &&
	dsa-update-apt-status -f

