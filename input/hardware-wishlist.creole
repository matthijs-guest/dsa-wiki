= Hardware Wishlist =

== new klecker (2018) ==

klecker is getting old and low on storage.  It also serves too many purposes.
We would like to replace it with two 1U boxes with 10GigE network, 10TB of
storage (after RAID), and possibly a VPN endpoint for access to out of band
management.

== new bytemark (2019?) ==

Probably want to move from a blade enclosure and MSA to traditional
rack-mounted servers.  Details TBD.

Plan as of early 2018 DSA sprint:

* get 2 to 4 machines
* get switch
* get oob
* migrate stuff from the blade center + MSA (let debconf/spi know), then get rid of blades + msa
* decide on routing (ask bm to route for us?) and get a 10GigE switch (cumulus?)
